FROM alpine:3.14

RUN apk update \
    && apk add --no-cache \
      bash \
      git \
      curl \
      python3 \
      openssh \
      py3-pip \
      zip \
    && git clone https://github.com/tfutils/tfenv.git /opt/tfenv \
    && ln -s /opt/tfenv/bin/* /usr/local/bin \
    && pip3 install --upgrade pip \
    && pip3 install awscli --upgrade \
    && rm -rf /var/cache/apk/*

SHELL ["/bin/sh", "-c"]
ENTRYPOINT ["/bin/sh", "-c"]
